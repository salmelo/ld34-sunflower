﻿using UnityEngine;
using System.Collections;

public class MaintainYOffset : MonoBehaviour
{
    public float yOffset;
    public Transform target;

    // Update is called once per frame
    void Update()
    {
        var pos = transform.position;
        pos.y = target.position.y + yOffset;
        transform.position = pos;
    }
}
