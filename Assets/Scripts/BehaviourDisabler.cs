﻿using UnityEngine;
using System.Collections;

public class BehaviourDisabler : MonoBehaviour
{
    public Behaviour[] behaviours;

    public void DisableNow()
    {
        foreach (var b in behaviours)
        {
            b.enabled = false;
        }
    }
}
