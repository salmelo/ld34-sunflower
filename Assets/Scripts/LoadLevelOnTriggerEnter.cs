﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadLevelOnTriggerEnter : MonoBehaviour
{
    public string levelToLoad;

    void OnTriggerEnter2D()
    {
        SceneManager.LoadScene(levelToLoad, LoadSceneMode.Single);
    }
}
