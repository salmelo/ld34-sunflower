﻿using UnityEngine;
using System.Collections;

public class Enabler : MonoBehaviour
{

    public GameObject target;

    public void EnableMe()
    {
        target.SetActive(true);
    }

}
