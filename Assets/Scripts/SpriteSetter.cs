﻿using UnityEngine;
using System.Collections;

public class SpriteSetter : MonoBehaviour
{

    public Sprite sprite;

    public void SetSprite()
    {
        GetComponent<SpriteRenderer>().sprite = sprite;
    }
}
