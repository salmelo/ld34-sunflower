﻿using UnityEngine;
using System.Collections;

public class BehaviourEnabler : MonoBehaviour
{
    public Behaviour[] behaviours;

    public void EnableNow()
    {
        foreach (var b in behaviours)
        {
            b.enabled = true;
        }
    }
}
