﻿using UnityEngine;
using System.Collections;

public class EnableBehavioursAtY : MonoBehaviour
{
    public float yTarget;
    public Behaviour[] behaviours;


    // Update is called once per frame
    void Update()
    {
        if (transform.position.y >= yTarget)
        {
            foreach (var b in behaviours)
            {
                b.enabled = true;
            }

            Destroy(this);
        }
    }
}
